import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class LogicTestApplication {

	public static void main(String[] args) {
		
		//create a test cart ---> List of cart items
		
		List<CartItem> cart = new ArrayList<>();
		
		//CartItem(productId, offerId, itemName, costPerPiece, quantity, originalItemPrice, effectiveItemPrice)		
		
		CartItem item1  = new CartItem (1,  1, "SHIRT 500", 500, 3, 1500, 1500);
		CartItem item2  = new CartItem (2,  2, "SHIRT 200", 200, 3, 600,  600 );
		CartItem item3  = new CartItem (3,  1, "SHIRT 300", 300, 4, 1200, 1200);
		CartItem item4  = new CartItem (4,  1, "SHIRT 100", 100, 1, 100,  100 );
		CartItem item5  = new CartItem (5,  3, "SHIRT 500", 500, 1, 500,  500 );
		CartItem item6  = new CartItem (6,  3, "SHIRT 200", 200, 6, 1200, 1200);
		CartItem item7  = new CartItem (7,  4, "SHIRT 100", 100, 2, 200,  200 );
		CartItem item8  = new CartItem (8,  4, "SHIRT 500", 500, 1, 500,  500 );
		CartItem item9  = new CartItem (9,  2, "SHIRT 300", 300, 1, 300,  300 );
		
		
		// Add items to List
		
		cart.add(item1);
		cart.add(item2);
		cart.add(item3);
		cart.add(item4);
		cart.add(item5);
		cart.add(item6);
		cart.add(item7);
		cart.add(item8);
		cart.add(item9); 

		
		System.out.println('\n'+"Before Processing");
		cart.forEach(System.out::println);
		
		// number of items = sum of quantities
		
		int num_of_items = cart.stream().mapToInt(CartItem::getQuantity).sum();
		System.out.println('\n'+"Number of items : "+num_of_items);

		//Map to group items in cart by offerId
		
		Map<Integer, List<CartItem>> cartByOffer = cart.stream()
				  .collect(Collectors.groupingBy(CartItem::getOfferId));
		
		System.out.println(cartByOffer);
		
		// One key value pair in the Map refers to one unique offer
		// Iterate through each offer and call the corresponding offerProcessing method
		
		for (Map.Entry<Integer, List<CartItem>> e : cartByOffer.entrySet()) {
			
			// offer id = 1 and 4 are two types of Buy X Get Y offers
			if (e.getKey() == 1 || e.getKey() == 4 ) {
				
				e.setValue(buyXGetYOfferProcessor(e.getValue()));
				
			}
			
			// offer id = 2 and 3 are two types of Buy X Get Y at Z % discount offers
			else if (e.getKey() == 2 || e.getKey() == 3 ) {
				
				e.setValue(buyXGetYAtZPercentDiscountOfferProcessor(e.getValue()));
				
			}
				
		}
		
		 
		// Print the Cart post processing the offers
		 List<CartItem> flat = 
				 cartByOffer.values().stream()
				        .flatMap(List::stream)
				        .collect(Collectors.toList());
		 
		 System.out.println('\n'+"After Processing");
		 flat.forEach(System.out::println);
			
	}
	
	// method for Buy X Get Y offer processing

	private static List<CartItem> buyXGetYOfferProcessor(List<CartItem> cart) {

		// Example: x = buy 2 , y = get 3 free
		
		int x = 2; int y = 3;
				
		int num_of_items = cart.stream().mapToInt(CartItem::getQuantity).sum();
		System.out.println('\n'+"Buy X get Y offer"+'\n'+"Number of total items : "+num_of_items);
		
		// Calculate how many items would be free 
		int num_of_free_items;
		
		
		/*
		 If x = 2, y = 3 and there are total of 7 items in cart for this offer
		 
		 Then, 
		 for every 5 items (i.e. x+y) ---> 3 items (i.e. y) items will be free
		 similarly, for every m * 5 items   --->  m * 3 items will be free
		 
		 But since this offer is buy X get UPTO Y items free...
		 
		 when number of items in cart go over 2 (i.e. x), one by one additional items will be free
		 Item 3 will be free...item 4 will be free...item 5 will be free
		 Hence any item greater than x will be free until it reaches (x+y)
		 then any item greater than (x+y)+x will be free until it reaches ((x+y)+x)+y)
		 */
		
		// if reminder > x then (reminder-x) items will be free
		
		if(num_of_items%(x+y) > x)
			num_of_free_items = ((num_of_items/(x+y))*y) + ((num_of_items%(x+y) - x));
		else
			num_of_free_items = ((num_of_items/(x+y))*y);
	
		System.out.println("Number of free items: "+num_of_free_items);
		
		//sort in ASC of costPerItem, ProductID (i.e. position in cart)
		
		cart.sort(Comparator.comparing(CartItem::getCostPerPiece).thenComparing(CartItem::getProductId));
		
		// c will track each row/index number in list
		// i will track number of items free (note: quantity 2 needs to be considered as two items)
		// j tracks quantity of items in each row
		
		int c = 0;
		for (int i = 0; i < num_of_free_items;) {
			
			for(int j = 0; j < cart.get(c).getQuantity() && i < num_of_free_items; j++	) {
				
				i++;
				
				// in each iteration, reduce cost of one free item from original price
				//set effective price = old effective price - cost of one unit 
				
				cart.get(c).setEffectiveItemPrice(cart.get(c).getEffectiveItemPrice() - cart.get(c).getCostPerPiece());
			}
			
			c++;			
		}
		
		return cart;
		
	}
	
	private static List<CartItem> buyXGetYAtZPercentDiscountOfferProcessor(List<CartItem> cart) {

		//Refer to comments in above buyXGetYOfferProcessor method
		// z = % off 
		// Buy 2 get 3 at 50% discount
		
		int x = 2; int y = 3; double z = 0.5;
				
		int num_of_items = cart.stream().mapToInt(CartItem::getQuantity).sum();
		System.out.println('\n'+"Buy X get Y at Z % discount offer"+'\n'+"Number of total items : "+num_of_items);
		
		int num_of_discount_items;
		
		if(num_of_items%(x+y) > x)
			num_of_discount_items = ((num_of_items/(x+y))*y) + ((num_of_items%(x+y) - x));
		else
			num_of_discount_items = ((num_of_items/(x+y))*y);
		
		System.out.println("Number of discounted items: "+num_of_discount_items);
		
		
				
		cart.sort(Comparator.comparing(CartItem::getCostPerPiece).thenComparing(CartItem::getProductId));
		
		int c = 0;
		for (int i = 0; i < num_of_discount_items;) {
			
			for(int j = 0; j < cart.get(c).getQuantity() && i < num_of_discount_items; j++	) {
				
				i++;
				cart.get(c).setEffectiveItemPrice(cart.get(c).getEffectiveItemPrice() - (z * cart.get(c).getCostPerPiece()));
			}
			
			c++;			
		}
		
		return cart;
		
	}
	

}
