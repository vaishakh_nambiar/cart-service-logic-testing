

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@EqualsAndHashCode(callSuper = false)
@Builder
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class CartItem {
	
	private int productId;
	private int offerId;
	private String itemName;
	private double costPerPiece; //unit price
	private int quantity;
	private double originalItemPrice;
	private double effectiveItemPrice;

}
